import QtQuick 2.0

import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.extras 2.0 as PlasmaExtras


Item {
    PlasmaExtras.ScrollArea {
        anchors.fill: parent
        ListView {
            id: mainList
            focus: true
            boundsBehavior: Flickable.StopAtBounds

            model: RssModel {
                id: rssModel
                source: "http://planetkde.org/rss20.xml"
            }

            delegate: PlasmaComponents.ListItem {
                PlasmaComponents.Label {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    height: implicitHeight

                    elide: Text.ElideRight
                    text: model.title

                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: Qt.openUrlExternally(model.link)
                }
            }
        }
    }

    PlasmaComponents.BusyIndicator {
        anchors.centerIn: parent
        //whilst the model is loading, stay visible
        //we use opacity rather than visible to force an animation
        opacity: rssModel.loading ? 1: 0

        //always spin. We hide when we're not wanted
        running: true

        Behavior on opacity {
            PropertyAnimation {
                //this comes from PlasmaCore
                duration: units.shortDuration
            }
        }
    }
}
