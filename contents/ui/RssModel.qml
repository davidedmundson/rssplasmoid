import QtQuick.XmlListModel 2.0

XmlListModel {
    id: feedModel
    query: "/rss/channel/item"
    property bool loading: status == XmlListModel.Loading

    XmlRole { name: "title"; query: "title/string()" }
    XmlRole { name: "link"; query: "link/string()" }
    XmlRole { name: "description"; query: "description/string()" }
}